﻿using Program.Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Automation;

namespace Program
{
    public  class Desktop
    {
        private readonly IntPtr _desktopHandle;
        private readonly List<string> _currentIconsOrder;

        

        public Desktop()
        {   
            /**
             * pobranie listy ikon
             */
            _desktopHandle = Win32.GetDesktopWindow(Win32.DesktopWindow.SysListView32);
            /**
             * szukanie po węzłach
             */
            AutomationElement el = AutomationElement.FromHandle(_desktopHandle);
            /**
             * poruszanie się po węzłach i pobieranie nazw do listy
             */
            TreeWalker walker = TreeWalker.ContentViewWalker;
            _currentIconsOrder = new List<string>();
            for (AutomationElement child = walker.GetFirstChild(el);
                child != null;
                child = walker.GetNextSibling(child))
            {
                _currentIconsOrder.Add(child.Current.Name);
            }
        }
        /**
         * pobieranie informacji na temat ilości ikon
         */
        private int GetIconsNumber()
        {
            
            return (int)Win32.SendMessage(_desktopHandle, Win32.LVM_GETITEMCOUNT, IntPtr.Zero, IntPtr.Zero);
        }
        /**
         * pobieranie pozycji
         */
        public NamedDesktopPoint[] GetIconsPositions()
        {
            uint desktopProcessId;
            Win32.GetWindowThreadProcessId(_desktopHandle, out desktopProcessId);

            IntPtr desktopProcessHandle = IntPtr.Zero;
            try
            {
                /**
                 * utworzenie uchwytu do procesu
                 */
                desktopProcessHandle = Win32.OpenProcess(Win32.ProcessAccess.VmOperation | Win32.ProcessAccess.VmRead |
                    Win32.ProcessAccess.VmWrite, false, desktopProcessId);
                
                return GetIconsPositions(desktopProcessHandle);
            }
            finally
            {
                if (desktopProcessHandle != IntPtr.Zero)
                { Win32.CloseHandle(desktopProcessHandle); }
            }
        }
        /**
         * pobranie pozycji wraz z nazwą
         */
        private NamedDesktopPoint[] GetIconsPositions(IntPtr desktopProcessHandle)
        {
            IntPtr sharedMemoryPointer = IntPtr.Zero;

            try
            {
                /**
                 * przydzielenie pamięci wirtualnej
                 */
                sharedMemoryPointer = Win32.VirtualAllocEx(desktopProcessHandle, IntPtr.Zero, 4096, Win32.AllocationType.Reserve | Win32.AllocationType.Commit, Win32.MemoryProtection.ReadWrite);

                return GetIconsPositions(desktopProcessHandle, sharedMemoryPointer);
            }
            finally
            {
                if (sharedMemoryPointer != IntPtr.Zero)
                {
                    Win32.VirtualFreeEx(desktopProcessHandle, sharedMemoryPointer, 0, Win32.FreeType.Release);
                }
            }

        }

        private NamedDesktopPoint[] GetIconsPositions(IntPtr desktopProcessHandle, IntPtr sharedMemoryPointer)
        {
            var listOfPoints = new LinkedList<NamedDesktopPoint>();

            var numberOfIcons = GetIconsNumber();
            /**
             * odczytanie poszczególnych ikon i przypisanie pozycji
             */
            for (int itemIndex = 0; itemIndex < numberOfIcons; itemIndex++)
            {
                uint numberOfBytes = 0;
                DesktopPoint[] points = new DesktopPoint[1];

                Win32.WriteProcessMemory(desktopProcessHandle, sharedMemoryPointer,
                    Marshal.UnsafeAddrOfPinnedArrayElement(points, 0),
                    Marshal.SizeOf(typeof(DesktopPoint)),
                    ref numberOfBytes);

                Win32.SendMessage(_desktopHandle, Win32.LVM_GETITEMPOSITION, itemIndex, sharedMemoryPointer);

                Win32.ReadProcessMemory(desktopProcessHandle, sharedMemoryPointer,
                    Marshal.UnsafeAddrOfPinnedArrayElement(points, 0),
                    Marshal.SizeOf(typeof(DesktopPoint)),
                    ref numberOfBytes);

                var point = points[0];
                listOfPoints.AddLast(new NamedDesktopPoint(_currentIconsOrder[itemIndex], point.X, point.Y));
            }

            return listOfPoints.ToArray();
        }
        /**
        * zapisywanie poszczególnych ikon w odpowiednich miejscach
        */
        public void SetIconPositions(IEnumerable<NamedDesktopPoint> iconPositions)
        {
            foreach (var position in iconPositions)
            {
                /**
                 * pobieranie nazwy danej ikony
                 */
                var iconIndex = _currentIconsOrder.IndexOf(position.Name);
                if (iconIndex == -1)
                { continue; }
                /**
                 * ustawianie ikony na danej pozycji
                 */
                Win32.SendMessage(_desktopHandle, Win32.LVM_SETITEMPOSITION, iconIndex, Win32.MakeLParam(position.X, position.Y));
            }
        }

     
    }
}
