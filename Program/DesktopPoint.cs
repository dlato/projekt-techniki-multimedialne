﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    /**
     * przechowywanie pozycji  X i Y danego punktu na ekranie
     */
    public struct DesktopPoint 
    {
        public int X;
        public int Y;

        public DesktopPoint(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
        
    }
    /**
     * przechowywanie pozycji X i Y oraz nazwy danego punktu na ekranie
     */
    public struct NamedDesktopPoint
    {
        public string Name;
        public int X;
        public int Y;


        public NamedDesktopPoint(string name, int x, int y)
        {
            this.Name = name;
            this.X = x;
            this.Y = y;
        }
    }
}
