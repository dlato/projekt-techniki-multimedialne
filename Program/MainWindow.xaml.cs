﻿using Program.Code;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Program
{
 
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DesktopRegistry _registry = new DesktopRegistry();
        private readonly Desktop _desktop = new Desktop();
        private readonly Storage _storage = new Storage();

      
        public MainWindow()
        {
           

            InitializeComponent();
        }
        /**
         * przycisk sortujący ikony i zapisapisujący pozycje na kopii
         */
    
        private void alfabetycznie_Click(object sender, RoutedEventArgs e)
        {
            var registryValues = _registry.GetRegistryValues();

            var iconPositions = _desktop.GetIconsPositions();

            var sortIconPosition = _storage.sortElements(iconPositions);
            _storage.SaveIconPositionsAlphabetics(sortIconPosition, registryValues);

           
        }
        /**
         * przycisk pobierający pozycje ikon z kopii i zapisujący je w oryginale
         */
        private void Przywroc_Click(object sender, RoutedEventArgs e)
        {
        
            var registryValues = _storage.GetRegistryValues();
          
            _registry.SetRegistryValues(registryValues);

            var iconPositions = _storage.GetIconPositions();


            _desktop.SetIconPositions(iconPositions);
        }
    }
}
