﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using static Program.DesktopPoint;

namespace Program
{
    public class Storage
    {
        /**
         * odczytywanie wartości z rejestru
         */
        public IDictionary<string, string> GetRegistryValues()
        {
            using (var storage = IsolatedStorageFile.GetUserStoreForAssembly())
            {
                if (storage.FileExists("Desktop") == false)
                { return new Dictionary<string, string>(); }

                using (var stream = storage.OpenFile("Desktop", FileMode.Open))
                {
                    using (var reader = XmlReader.Create(stream))
                    {
                        var xDoc = XDocument.Load(reader);

                        return xDoc.Root.Element("Registry").Elements("Value")
                            .ToDictionary(el => el.Element("Name").Value, el => el.Element("Data").Value);
                    }
                }
            }
        }
        /**
         * sortowanie po nazwie
         */
        public NamedDesktopPoint[] sortElements(NamedDesktopPoint[] iconPositions)
        {
            NamedDesktopPoint[] nowaKopia = new NamedDesktopPoint[iconPositions.Count()];
            Array.Copy(iconPositions, nowaKopia, iconPositions.Count());

            var newIconPosition = iconPositions;
            Array.Sort(newIconPosition, (x, y) => String.Compare(x.Name, y.Name));
            for (int i = 0; i < newIconPosition.Count(); i++)
            {
                nowaKopia[i].Name = iconPositions[i].Name;

            }
            return nowaKopia;
        }
        /**
         * utworzenie dokumentu XML zawierającego informacje o ikonach i skopiowanie do izolowanego miejsca
         */
        public void SaveIconPositionsAlphabetics(IEnumerable<NamedDesktopPoint> iconPositions, IDictionary<string, string> registryValues)
        {
            var xDoc = new XDocument(
                new XElement("Desktop",
                    new XElement("Icons",
                        iconPositions.Select(p => new XElement("Icon",
                            new XAttribute("x", p.X),
                            new XAttribute("y", p.Y),
                            new XText(p.Name)))),
                    new XElement("Registry",
                        registryValues.Select(p => new XElement("Value",
                            new XElement("Name", new XCData(p.Key)),
                            new XElement("Data", new XCData(p.Value)))))));
        
            using (var storage = IsolatedStorageFile.GetUserStoreForAssembly())
            {
                if (storage.FileExists("Desktop"))
                { storage.DeleteFile("Desktop"); }

                using (var stream = storage.CreateFile("Desktop"))
                {
                    using (var writer = XmlWriter.Create(stream))
                    {
                        xDoc.WriteTo(writer);
                    }
                }
            }
        }


        /**
         * pobranie informacji o pozycji ikon z dokumentu XML
         */
        public IEnumerable<NamedDesktopPoint> GetIconPositions()
        {
            using (var storage = IsolatedStorageFile.GetUserStoreForAssembly())
            {
                if (storage.FileExists("Desktop") == false)
                { return new NamedDesktopPoint[0]; }

                using (var stream = storage.OpenFile("Desktop", FileMode.Open))
                {
                    using (var reader = XmlReader.Create(stream))
                    {
                        var xDoc = XDocument.Load(reader);

                        var result = 
                         xDoc.Root.Element("Icons").Elements("Icon")
                            .Select(el => new NamedDesktopPoint(el.Value, int.Parse(el.Attribute("x").Value), int.Parse(el.Attribute("y").Value)))
                            .ToList();
                        return result;
                    }
                }
            }
        }
    }
}
