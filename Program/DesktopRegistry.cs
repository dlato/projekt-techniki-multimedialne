﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Program
{
    public class DesktopRegistry
    {
        /**
         * pod tym adresem przypisanym do KeyName Windows przechowyje pozycję ikon 
         */
        private const string KeyName = @"Software\Microsoft\Windows\Shell\Bags\1\Desktop";

        private readonly BinaryFormatter _formatter = new BinaryFormatter();
        /**
         * pobieranie wartości słownika klucz-wartość
         */
        public IDictionary<string, string> GetRegistryValues()
        {
            using (var registry = Registry.CurrentUser.OpenSubKey(KeyName))
            {
                return registry.GetValueNames().ToDictionary(n => n, n => GetValue(registry, n));
            }
        }
        /**
         * odczytywanie wartości o danej nazwie, serializacja, konwertowanie na base64
         */
        private string GetValue(RegistryKey registry, string valueName)
        {
            var value = registry.GetValue(valueName);
            if (value == null)
            { return string.Empty; }

            using (var stream = new MemoryStream())
            {
                _formatter.Serialize(stream, value);

                var bytes = stream.ToArray();

                return Convert.ToBase64String(bytes);
            }
        }

        /**
         * przypiswyanie do klucza odpowiednich wartości
         */
        public void SetRegistryValues(IDictionary<string, string> values)
        {
            using (var registry = Registry.CurrentUser.OpenSubKey(KeyName, true))
            {
                foreach (var item in values)
                {
                    registry.SetValue(item.Key, GetValue(item.Value));
                }
            }
        }
        /*
         * konwertowanie na string, deserializacja
         */
        private object GetValue(string stringValue)
        {
            if (string.IsNullOrEmpty(stringValue))
            { return null; }

            var bytes = Convert.FromBase64String(stringValue);

            using (var stream = new MemoryStream(bytes))
            {
                return _formatter.Deserialize(stream);
            }
        }
    }
}

